# LazyHost

## Goals:
- Make self-hosting web-apps possible for "average" tech-skilled users (no admins / no programmers)
- Have many good integrated, easy to install & convenient to use apps 
- Keep maintance work low
- Be attractive to contributers

## Why?
All existing self-hosting solutions have some of the following limitations.

For users:
- too hard for "normal" users
- few or bad integrated / bad maintained apps
- too unconvenient, i.e. no GUI, no single-sign-on (SSO)
- too buggy

For developers:
- too many / suboptimal technologies
- too messy structured, bad docs
- too much code -> reinventing the wheel
=> hard to maintain, hard to find contributers

## How?
- Find & reuse as many existing partial solutions as possible
- Glue them together in a short, consistent and readable way
- Stick to the KISS-principle
- Use a modern but mature enough tech-stack with as few technologies as possible
	- Make good design trade-offs BEFORE implementing anything
- Have good project & code docs
- Build a CI-pipeline

---
## decision making process
Necessary tech-stack:
1. OS
2. Configuration-tech
3. proxy
	- SSL
	- SSO
4. container-tech
5. web-apps
8. GUI

### OS
Support a single OS to reduce maintainance work:

Debian:
  pros:
- most stable
    cons:
- older software-versions: easy to fix
- system upgrade needed approx. every 5 years (no rolling release)
Arch:
  pros:
  - rolling release
    cons:
  - too hard to install on a server
  - stability?

---
### config-tech
Use ONE config syntax for as much as possible.
YAML
	pros:
    - Predominant config syntax for the "cloud"
    - Can be used for system, proxy & app config
    - Broad editor support
	cons:
    - Picky about indention, spacing & quoting

Use a config-tool for readability, consistency and idempotency reasons:
Ansible:
	pros:
    - Easiest to learn, no programming necessary
    - Very powerful
    - very readable config (YAML)
    - no agent needed
	cons:
    - ?

### proxy
Traefik:
	pros:
    - plays nice with containerized apps
    - auto-SSL out-of-the box
    - easy SSO-solution with 3rd party package (Authelia)
    - Kubernetes-ready in case of project port to k3s
    cons:
    - complex to learn
Caddy:
	pros:
    - easy
    - auto-SSL
	cons:
    - SSO?
    - not k3s-ready
    - stability?

---
### Container
Docker:
	pros:
    - by far the leading tech
    - many high-quality docker-compose apps out there
	cons:
    - needs daemon running as root
    - non-root linux user added to docker group may be still a security risk
Podman:
	pros:
    - no daemon
    - rootless containers
	cons:
    - app compatibility?
    - not k3s portable?
    - docker-compose support?

### Web-Apps
- Project owners support only docker images from linuxserver.io 
- Let users become maintainers of their favorite apps

### Web-UI
Needed features:
- 1-click-installs via icons
No existing solution found.

Tech-stack needed:
1. programming language
2. database
3. backend web framework / request handler
4. frontend web framework

---
#### Programming language
Python
	pros:
    - No.1 in popularity
    - easy to learn
    - highly readable
	cons:
    - slow (CPU bound)

#### database
SQL-databases should be avoided because of the need for migrations => not manageable for end-users.
MongoDB:
	pros:
    - easy queries
    - fast
    - scalable
    - good framework integration
    - useable with or without database scheme
	cons:
    - ?

#### Backend framework
Flask:
	pros:
    - minimal "Microframework"
    - mature and popular
	cons:
    - no batteries included, custom templates, db etc. needed
Django:
	pros:
    - most mature
    - "batteries included"
	cons:
    - only SQL => KO-criteria
FastAPI:
	pros:
    - async request handling
    - stable enough
	cons:
    - very young
    - not as popular as the others
