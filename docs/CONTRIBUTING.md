### LazyHost Design Goals

Make self-hosting web apps feasable for average tech-skilled people without any prior knowledge of server administration, programming or linux terminal commands. At the same time make it easily customizable for more advanced users.

- Follow the "keep it simple, stupid (KISS)"-priciple.
- Reuse existing partial solutions
- Glue them together in a clean, concise & consistent way
- Choose sane defaults
- Have good docs

### How does LazyHost work?

First of all, LazyHost would not have been possible without standing on the shoulder of giants:

- linuxserver.io: Maintaining high quality dockerized webapps
- dockSTARTer: Providing a cli too install those apps conveniently

LazyHost then automates common additional tasks in self-hosting like a ready-to-use configuration of the server and web-proxy with SSL & single-sign-on, as well as a web-UI for starting and installing apps.

YAML has become the predominant syntax for "cloud configuration". Besides aspects of maturity and simplicity, tools in this project have been chosen to be configured with YAML. For automating the configuaration of the server and the tools, Ansible is used for configuration management and docker-compose for container-orchestration, both also using YAML.
