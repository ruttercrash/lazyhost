#!/usr/bin/env bash

# strict mode
set -euo pipefail
IFS=$'\n\t'

# constants
readonly GREEN='\033[0;32m'
readonly RED='\033[0;31m'
readonly CLEAR='\033[0m'
readonly FALSE=1

# vars
LAZYHOST_DOMAIN=""
LAZYHOST_EMAIL=""

# trace errors & cleanup
err_report() {
    echo "${RED}Error on line $1${CLEAR}"
}
trap 'err_report $LINENO' ERR

# helper functions
is_dir() {
    local dir=$1
    [[ -d $dir ]]
}

is_file() {
    local file=$1
    [[ -f $file ]]
}

read_dotenv() {
        set -o allexport; source .env; set +o allexport
}

is_env_set() {
    for arg in "$@"; do
        [ -n "$arg" ] \
            || return $FALSE
    done
}

main() {
    is_file .env \
        && read_dotenv

    if ! is_env_set "$LAZYHOST_DOMAIN" "$LAZYHOST_EMAIL"; then
        read -rp 'Domain name (e.g. example.org): ' LAZYHOST_DOMAIN
        read -rp 'Email: ' LAZYHOST_EMAIL
    fi

    echo -e "${GREEN}Installing packages...${CLEAR}"
    apt update -y && apt install -y \
        curl git ruby python3-pip python3-venv figlet lolcat sudo

    python3 -m venv lazy-env
    # shellcheck source=/dev/null
    source ./lazy-env/bin/activate
    pip install ansible

    echo -e "${GREEN}Cloning LazyHost repo...${CLEAR}"
    is_dir lazyhost \
        && rm -rf lazyhost
    git clone https://gitlab.com/ruttercrash/lazyhost.git

    echo -e "${GREEN}Preparing server for LazyHost...${CLEAR}"
    readonly LAZYHOST_SH=/etc/profile.d/lazyhost.sh
    touch $LAZYHOST_SH
    echo "export LAZYHOST_DOMAIN=${LAZYHOST_DOMAIN}" >> $LAZYHOST_SH
    echo "export LAZYHOST_EMAIL=${LAZYHOST_EMAIL}" >> $LAZYHOST_SH
    # shellcheck source=/dev/null
    source /etc/profile.d/lazyhost.sh

    cd lazyhost \
        && ansible-playbook ./lazyhost/ansible/playbooks/root_setup.yml

    echo -e "${GREEN}Installing dockSTARTer...${CLEAR}"
    curl https://raw.githubusercontent.com/GhostWriters/DockSTARTer/master/main.sh | su -l lazyhost

    echo -e "${GREEN}Configuring LazyHost...${CLEAR}"
    ansible-playbook ./lazyhost/ansible/playbooks/lazyhost_setup.yml
    
    echo -e "${GREEN}Starting containers...${CLEAR}"
    su -l lazyhost -c 'ds -c up'

    echo -e "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    su -l lazyhost -c 'figlet -f slant LazyHost | lolcat -a -S 20 -F 0.3 -d 10'
    echo -e "${RED}V0.0.1a${CLEAR}\n\n"
    echo -e "${GREEN}SUCCESS! :) Goin' for reboot... ${CLEAR}"
    reboot
}
main