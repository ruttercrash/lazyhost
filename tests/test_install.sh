#!/usr/bin/env bash


### TO BE RUN FROM LAZYHOST ROOT DIR ###
# TODO: Integrate this into new_testserver(..).py


# strict mode
set -euo pipefail
IFS=$'\n\t'

# trace errors & cleanup
err_report() {
    echo "Error on line $1"
    # hcloud server delete lazy
}
trap 'err_report $LINENO' ERR

# read 1st cmd arg
[ "$#" -eq 0 ] \
  && ARG="" \
  || ARG=$1

read_dotenv() {
    set -o allexport; source .env; set +o allexport
}

main() {
    tests/new_testserver_and_dns_update.py "$ARG"
    sleep 1

    read_dotenv

    echo "Removing ${TESTSERVER_IP} from known_hosts"
    ssh-keygen -R "${TESTSERVER_IP}"

    scp -o 'StrictHostKeyChecking no' ./install.sh root@"${TESTSERVER_IP}":/root/install.sh
    scp -o 'StrictHostKeyChecking no' ./tests/test.env root@"${TESTSERVER_IP}":/root/.env
    ssh -o 'StrictHostKeyChecking no' root@"${TESTSERVER_IP}" "bash ./install.sh"
}
main