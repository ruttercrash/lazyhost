#!/usr/bin/env python
import subprocess
from dataclasses import dataclass
import json
import sys
import time

import dotenv
import fabric
import fabric.transfer
import hcloud
from hcloud.server_types.domain import ServerType
from hcloud.images.domain import Image
import requests
from paramiko.ssh_exception import NoValidConnectionsError
from rich.console import Console


@dataclass
class DnsRecord:
    id: str
    name: str


DNS_ZONE_ID = "aL3VEerykwGnoTVVHN7PwJ"  # lazyhost.eu
TLD_RECORD = DnsRecord(id='1b5425462dcb2503eb74fc60968d4459', name='test')  # test.lazyhost.eu
WILDCARD_RECORD = DnsRecord(id='08a8027577a712f10d7a7301b7e69ebc', name='*.test')  # *.test.lazyhost.eu
ENV = dotenv.dotenv_values('.env')
console = Console()
DEBUG = False
NEW_SERVER = False
if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        if arg in ('--debug', 'd'):
            DEBUG = True
        if arg in ('--new', '-n'):
            NEW_SERVER = True


def shell_run(cmd):
    try:
        result = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as err:
        console.print("ERROR: ", err)
    else:
        console.print(result.stdout.decode("utf-8"))


def create_server(hcloud_client):
    server = hcloud_client.servers.create(
        'lazy',
        ServerType('cx11'),
        Image('debian-11'),
        ssh_keys=hcloud_client.ssh_keys.get_all()
    ).server
    console.log('created server', server.id,
                'IP', server.public_net.ipv4.ip)

    return server


def delete_servers(hcloud_client):
    if servers := hcloud_client.servers.get_all():
        for server in servers:
            console.log('deleted server', server.delete().id,
                        'IP', server.public_net.ipv4.ip)


def update_dns(record, server_ip):
    url = f'https://dns.hetzner.com/api/v1/records/{record.id}'
    headers = {
        "Content-Type": "application/json",
        "Auth-API-Token": ENV['HCLOUD_DNS_TOKEN'],
    }
    data = json.dumps({
        "value": server_ip,
        "ttl": 60,
        "type": "A",
        "name": record.name,
        "zone_id": DNS_ZONE_ID
    })
    console.log(f'Updating DNS-records to new IP {server_ip}...')
    if DEBUG:
        console.log('[dim white][DEBUG] Putting[/dim white]', url, data)
    if (response_code := requests.put(url=url, headers=headers, data=data)
            .status_code) != 200:
        raise Exception(f'Failed to set new IP in DNS-record! Response {response_code}')


def is_ssh_ready(ssh_connection):
    try:
        ssh_connection.run('echo')
        return True
    except NoValidConnectionsError:
        return False


def server_ready_greeting(hcloud_client):
    console.log(f'[bold]SERVER[yellow] {hcloud_client.servers.get_all()[0].name} [/yellow] READY[/bold]')


def main():
    hcloud_client = hcloud.Client(token=ENV['HCLOUD_TOKEN'])

    if NEW_SERVER:
        delete_servers(hcloud_client)
    if not (server := hcloud_client.servers.get_all()):
        server = create_server(hcloud_client)
    else:
        server = server[0]

    for record in (TLD_RECORD, WILDCARD_RECORD):
        update_dns(record, server_ip := server.public_net.ipv4.ip)

    console.print('Waitin till server is ready for SSH...', end='', style='yellow')
    ssh_connection = fabric.Connection(f'root@{server_ip}')
    ssh_transfer = fabric.transfer.Transfer(ssh_connection)

    while not is_ssh_ready(ssh_connection):
        console.print('.', end='', style='yellow')
        time.sleep(0.5)
    server_ready_greeting(hcloud_client)

    shell_run(f"ssh-keygen -R {server_ip}")
    ssh_transfer.put(local="./install.sh", remote="/root/install.sh")
    ssh_transfer.put(local="./tests/test.env", remote="/root/.env")
    ssh_connection.run("bash ./install.sh")


if __name__ == '__main__':
    main()
