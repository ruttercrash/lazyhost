#!/usr/bin/env bash

readonly TRUE=0
readonly FALSE=1

is_env_set() {
    for arg in "$@"; do
        [ -n "$arg" ] \
            && echo "$arg" \
            || return $FALSE
    done
}

if ! is_env_set "$LAZYHOST_DOMAIN" "$LAZYHOST_EMAIL"; then
read -rp 'Domain name (e.g. example.org): ' LAZYHOST_DOMAIN
read -rp 'Email: ' LAZYHOST_EMAIL
fi

if ! is_env_set "$LAZYHOST_DOMAIN" "$LAZYHOST_EMAIL"; then
read -rp 'Domain name (e.g. example.org): ' LAZYHOST_DOMAIN
read -rp 'Email: ' LAZYHOST_EMAIL
fi
