# LazyHost

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Selfhosting (almost) effortless. [Documentation](https://lazyhost.de/)

Makes running your own web-apps feasable for all & customizable for advanced users.

[[_TOC_]]

## Project status

**EARLY ALPHA, WORK-IN-PROGRESS!**

## Features

- [x] Plenty popular high quality up-to-date apps (maintained by [linuxserver.io](https://www.linuxserver.io/), fetched with [dockSTARTer](https://github.com/GhostWriters/DockSTARTer/))
- [ ] 1-click-install backend for apps (by LazyHost)
- [ ] Nice homescreen to start & choose your apps ([homer](https://github.com/bastienwirtz/homer))
- [ ] Proxy with automatic SSL-encryption for your apps ([traefik](https://github.com/traefik/traefik))
- [ ] Single-Sign-On (SSO): Only login once to use all your apps ([Authelia](https://github.com/authelia/authelia))
- [ ] Basic server security config by default (firewall, rate limit on ports)
- [x] LazyHost install by copy & paste 1 line of code
- [x] LazyHost is easily hackable for your preferred customizations

## Requirements

A physical or virtual private server (x86-based) meeting the following criteria:
- full root access
- running current Debian 11 (bullseye) linux
- docker-capable
- dedicated external IP-address

A registered (sub-)domain with wildcard support, appropriate DNS entries pointing to your server, e. g.:

| TYPE | HOST | ANSWER |
| ------ | ------ | ------ |
| A | yourdomain.com | your.server.ip.address |
| A | *.yourdomain.com | your.server.ip.address |

## Installation

``` bash
bash -c "$(curl -fsSL https://gitlab.com/ruttercrash/lazyhost/-/raw/main/install.sh)"
```
Answer all prompts (username, password, domain & email).

Reboot.

## Usage

Enjoy your homescreen at yourdomain.com to start your apps.
Choose your apps by clicking INSTALL on the menu bar.

## Support

Please submit your

- [Feature Request](https://gitlab.com/ruttercrash/lazyhost/-/issues/new) or
- [Bug Report](https://gitlab.com/ruttercrash/lazyhost/-/issues/new)

and choose the corresponding template from Description drop-down.

## Roadmap

TODO: Port LazyHost to lightweight Kubernetes (k3s) to make it scalable.

## Contributing

Contributions of any kind are appreciated (see [CONTRIBUTING](lazyhost/CONTRIBUTING.md)).

## Code of Conduct

LazyHost community aims to be open, including, welcoming and polite -especially in case of conflicts- (see [CODE OF CONDUCT](lazyhost/CODE_OF_CONDUCT.md) )

## License

[Apache License 2.0](LICENSE.md)

Copyright (C) 2022 Marco Heins