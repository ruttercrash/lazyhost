#!/usr/bin/env bash

# strict mode
set -euo pipefail
IFS=$'\n\t'

# constants
export readonly GREEN='\033[0;32m'
export readonly RED='\033[0;31m'
export readonly CLEAR='\033[0m'

# duplicate stdout
exec 5>&1
print_var() {
    tee >(cat - >&5)
}

# trace errors & cleanup
err_report() {
    echo "${RED}Error on line $1${CLEAR}"
    # hcloud server delete lazy
}
trap 'err_report $LINENO' ERR

# helper functions
continue_on_err() {
    return 0
}

is_empty() {
    local var=$1
    [[ -z $var ]]
}

is_file() {
    local file=$1
    [[ -f $file ]]
}

is_dir() {
    local dir=$1
    [[ -d $dir ]]
}


# shellcheck source=./lazyhost/bash_header.sh
# source ./lazyhost/bash_header.sh


# functions
read_dotenv() {
    if is_file .env; then
        set -o allexport; source .env; set +o allexport; fi
}


create_server() {  # return hcloud create server output
    local hcloud_output

    echo -e "${GREEN}Creating server...${CLEAR}"
    hcloud_output="$(hcloud server create --name lazy --type cx11 --image debian-11 --location fsn1 --ssh-key barrios@thinkpad \
        | tee >(cat - >&5))"

    echo "${hcloud_output}"
}


parse_ip_address() {  # return ip address
    local hcloud_output=$1

    ip_address_pattern='(([0-9]{1,3}[\.]){3}[0-9]{1,3})'
    [[ "$hcloud_output" =~ $ip_address_pattern ]]
    local server_ip="${BASH_REMATCH[0]}"

    echo "${server_ip}"
}


repeat_login_til_script_runs() {
    local ssh_output

    echo -e "${GREEN}Trying to login...${CLEAR}"

    ssh_output=$(run_ssh_cmd)
    while [[ ${ssh_output} =~ "No route to host"|"Connection refused"|"Connection timed out" ]]; do
        # copy test.env to prevent user prompt
        scp .test.env root@lazyhost.eu:/root/.env

        ssh_output=$(run_ssh_cmd); done
}


run_ssh_cmd() {  # return ssh output
    local ssh_output

    echo -e "${GREEN}Running setup script...${CLEAR}"

    ssh_output=$(ssh -o 'StrictHostKeyChecking no' root@lazyhost.eu \
        "bash -s" < ./install.sh \
        2>&1 \
        | tee >(cat - >&5) \
        || continue_on_err)

    echo "${ssh_output}"
}


del_server_if_exists() {
    if [[ $(hcloud server list) =~ "lazy" ]]; then
        hcloud server delete lazy; fi
}


update_dns_record() {
    local server_ip=$1

    curl -X "PUT" "https://dns.hetzner.com/api/v1/records/$DNS_RECORD_ID" \
    -H 'Content-Type: application/json' \
    -H 'Auth-API-Token: '$DNS_TOKEN \
    -d $'{
        "value": "'"$server_ip"'",
        "ttl": 600,
        "type": "A",
        "name": "@",
        "zone_id": "'$DNS_ZONE_ID'"
    }' \
    | jq

}


main() {
    local  server_output=''
    local  server_ip=''

    read_dotenv

    while [[ $server_ip != '168.119.51.230' ]]; do \
        del_server_if_exists
        server_output=$(create_server)
        server_ip=$(parse_ip_address "${server_output}")
    done
    # update_dns_record "${server_ip}"
    ssh-keygen -R lazyhost.eu
    repeat_login_til_script_runs
}

main